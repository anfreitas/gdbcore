from pydq.local import SQLite
from pydq import TIME_FORMAT
from pydq.helpers import DqConfig

from datetime import datetime, timedelta
import os
import json
import sys
from argparse import ArgumentParser


class GDBC:
    def __init__(self):
        parser = ArgumentParser(usage='gdbc <init|upload|download|copy|get> [args]')
        parser.add_argument('command')
        args = parser.parse_args(sys.argv[1:2])
        getattr(self, args.command)()

    def init(self):
        parser = ArgumentParser(usage='gdbc init -g <gid> -c <customer> [-t initial_timestamp] [-s sites]')
        parser.add_argument('-g', '--gid', required=True)
        parser.add_argument('-c', '--customer', required=True)
        parser.add_argument('-a', '--api-key', required=True)
        parser.add_argument('-t', '--ts', default=datetime.strftime(datetime.utcnow() - timedelta(days=7), TIME_FORMAT))
        parser.add_argument('-s', '--sites', nargs='*', default=[])
        args = parser.parse_args(sys.argv[2:])

        gid = args.gid
        customer = args.customer
        api_key = args.api_key
        site_names = args.sites
        ts = args.ts

        print('Creating initial config for %s with sites %s' % (gid, site_names))

        if hasattr(sys, 'frozen'):
            config_path = os.path.join(sys._MEIPASS, 'initial_setups', '%s.json' % customer)
        else:
            config_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'initial_setups', '%s.json' % customer)
        with open(config_path) as f:
            initial_setup = json.loads(f.read())

        if '*' in site_names:
            site_names = initial_setup['sites'].keys()

        if gid not in site_names and not gid.startswith('_'):
            site_names.insert(0, gid)

        for site in site_names:
            with DqConfig('GDBShare', SQLite, '_'.join((site, 'config')), {}) as share_config:
                share_config.update({
                    'endpoints': initial_setup['sites'][site]['shareEndpoints']
                })

            with DqConfig('SMBShare', SQLite, '_'.join((site, 'config')), {}) as share_config:
                share_config.update({
                    'endpoints': initial_setup['sites'][site]['smbEndpoints']
                })

            with DqConfig('Cleaner', SQLite, '_'.join((site, 'config')), {}) as cleaner_config:
                queues = ['%s_GCTide' % site]
                if 'sensors' in initial_setup['sites'][site]:
                    queues.insert(0, '%s_%s' % (site, initial_setup['sites'][site]['sensors']))
                cleaner_config.update({'queues': queues})

            if 'tidalStationId' in initial_setup['sites'][site]:
                with DqConfig('GCTide', SQLite, '_'.join((site, 'config')), {}) as gctide_config:
                    gctide_config.update({'station_id': initial_setup['sites'][site]['tidalStationId']})

        with SQLite('__system__') as dq:
            dq.put({'qid': 'log_url', 'ts': ts, 'val': initial_setup['logUrl']})
            if 'logApiKey' in initial_setup:
                dq.put({'qid': 'log_api_key', 'ts': ts, 'val': initial_setup['logApiKey']})
            dq.put({'qid': 'sites', 'ts': ts, 'val': json.dumps({k: v for k, v in initial_setup['sites'].items() if k in site_names})})
            dq.put({'qid': 'gid', 'ts': ts, 'val': gid})
            dq.put({'qid': 'smb_api_key', 'ts': ts, 'val': api_key})

    def copy(self):
        from pydq.cloud import DynamoDB
        parser = ArgumentParser(usage='gdbc copy <source> <destination> [-s sites]')
        parser.add_argument('source')
        parser.add_argument('destination')
        parser.add_argument('-s', '--sites', nargs='*', default=['*'])
        args = parser.parse_args(sys.argv[2:])
        source = args.source
        destination = args.destination
        site_names = args.sites

        if source == 'DynamoDB':
            dq_source = DynamoDB
            queues = DynamoDB.list_all()
        else:
            dq_source = lambda name: SQLite(name, db_file=source)
            queues = SQLite.list_all(db_file=source)

        if destination == 'DynamoDB':
            dq_destination = DynamoDB
        else:
            dq_destination = lambda name: SQLite(name, db_file=destination)

        if '*' not in site_names:
            queues = [q for q in queues if q.rsplit('_', maxsplit=1)[0].replace('_', ' ') in site_names]
        for queue in queues:
            with dq_source(queue) as dq:
                items = dq(end_time=datetime(5000, 12, 31)).get_all()
            with dq_destination(queue) as dq:
                dq.put_all(items)

    def upload(self):
        parser = ArgumentParser(usage='gdbc upload [source]')
        parser.add_argument('source', nargs='?', default=os.path.join(os.getcwd(), SQLite.FILE_NAME))
        parser.add_argument('-s', '--sites', nargs='*', default=['*'])
        args = parser.parse_args(sys.argv[2:])
        sys.argv = sys.argv[:2] + [args.source, 'DynamoDB', '-s'] + args.sites
        self.copy()

    def download(self):
        parser = ArgumentParser(usage='gdbc download [destination]')
        parser.add_argument('destination', nargs='?', default=os.path.join(os.getcwd(), SQLite.FILE_NAME))
        parser.add_argument('-s', '--sites', nargs='*', default=['*'])
        args = parser.parse_args(sys.argv[2:])
        sys.argv = sys.argv[:2] + ['DynamoDB', args.destination, '-s'] + args.sites
        self.copy()

    def get(self):
        parser = ArgumentParser(usage='gdbc get [source] [-l limit] -b <bname> [-f] [-q qids]')
        parser.add_argument('source', nargs='?', default=os.path.join(os.getcwd(), SQLite.FILE_NAME))
        parser.add_argument('-l', '--limit', default=10)
        parser.add_argument('-b', '--bname', required=True)
        parser.add_argument('-f', '--forget', action='store_true', default=False)
        parser.add_argument('-q', '--qids', nargs='*', default=['*'])
        args = parser.parse_args(sys.argv[2:])

        source = args.source
        limit = int(args.limit)
        bname = args.bname
        qids = args.qids
        forget = args.forget

        if source == 'DynamoDB':
            from pydq.cloud import DynamoDB
            dq_source = DynamoDB
        else:
            dq_source = lambda name: SQLite(name, db_file=source)

        if '*' in qids:
            qids = [None]

        items = []
        with dq_source(bname) as dq:
            for qid in qids:
                items.extend(list(dq(qid=qid, limit=limit).get_all(forget=forget)))

        print(json.dumps(items, indent=2))

    def put(self):
        parser = ArgumentParser(usage='gdbc put [destination] -b <bname> -i <json dict or list>')
        parser.add_argument('destination', nargs='?', default=os.path.join(os.getcwd(), SQLite.FILE_NAME))
        parser.add_argument('-b', '--bname', required=True)
        parser.add_argument('-i', '--items', default='[]')
        args = parser.parse_args(sys.argv[2:])

        destination = args.destination
        bname = args.bname
        items = args.items

        if destination == 'DynamoDB':
            from pydq.cloud import DynamoDB
            dq_dest = DynamoDB
        else:
            dq_dest = lambda name: SQLite(name, db_file=destination)

        if items.startswith('@'):
            with open(items[1:]) as f:
                items = f.read()
        items = json.loads(items)
        if isinstance(items, dict):
            items = [items]

        with dq_dest(bname) as dq:
            dq.put_all(items)


if __name__ == '__main__':
    g = GDBC()
