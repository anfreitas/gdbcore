from pydq import TIME_FORMAT
from gdbcore.broker import Broker

from datetime import datetime, timedelta
import logging


class Cleaner(Broker):
    NAME = 'Cleaner'
    INTERVAL = 86400     # once a day

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.logger = logging.getLogger('GDB')
        self.default_config.update({'cursor': (datetime.utcnow() - timedelta(days=7)).strftime(TIME_FORMAT),
                                    'ttl': 7, 'queues': []})

    def get_data(self):
        with self.config() as config:
            ttl = config['ttl']
            queues = config['queues']

        end_time = datetime.utcnow() - timedelta(days=ttl)
        for queue in queues:
            with self.dq_provider(queue) as dq:
                deleted = dq(end_time=end_time).get_all(forget=True)
                self.logger.info('Removed %i items from %s' % (len(deleted), queue))
