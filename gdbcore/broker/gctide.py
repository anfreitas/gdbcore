from gdbcore.broker import Broker
from pydq import TIME_FORMAT

from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import numpy as np
import pytz


class GCTideBroker(Broker):
    NAME = 'GCTide'
    INTERVAL = 259200  # every 3 days

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.default_config.update({'cursor': (datetime.utcnow() - timedelta(days=7)).strftime(TIME_FORMAT),
                                    'station_id': '',
                                    'time_offset': 0})

    def get_data(self):
        with self.config() as config:
            cursor, station_id, time_offset = config['cursor'], config['station_id'], config['time_offset']

        data_arr = []
        url = 'http://www.tides.gc.ca/eng/station'
        params = {
            'type': '0',
            'date': datetime.strptime(cursor, TIME_FORMAT).strftime('%Y/%m/%d'),
            'sid': station_id,
            'tz': 'UTC',
            'pres': '2'}

        resp = requests.get(url=url, params=params)
        if resp.status_code != 200:
            raise Exception('Failed retrieving GC tide table. Status code: %i' % resp.status_code)

        # parse request
        soup = BeautifulSoup(resp.text, 'html.parser')
        data = soup.find(class_='stationTextData')
        if data is None:
            raise Exception('Failed retrieving GC tide table. Empty response.')

        # append to data_arr
        for record in data:
            in_record = record.string.strip().split(';')
            if len(in_record) < 4:
                continue
            naive_cur_date = datetime.strptime(in_record[0] + ' ' + in_record[1], '%Y/%m/%d %H:%M:%S')
            cur_date = pytz.utc.localize(naive_cur_date, is_dst=None)

            data_arr.append([datetime.strftime(cur_date, TIME_FORMAT), in_record[2].strip('(m)')])

        interpolated_data = self.interpolate(data_arr, time_offset)

        with self.dq() as dq:
            dq.put_all([{'qid': self.broker_id(station_id), 'ts': item[0],
                         'val': item[1]} for item in interpolated_data])

        with self.config() as config:
            config['cursor'] = datetime.strftime(datetime.utcnow(), TIME_FORMAT)

    @staticmethod
    def interpolator(strx_1, strx_2, stry_1, stry_2, time_offset=0.0):
        x1 = datetime.strptime(strx_1, TIME_FORMAT)
        x2 = datetime.strptime(strx_2, TIME_FORMAT)
        y1 = float(stry_1)
        y2 = float(stry_2)

        delta_x = int((x2 - x1).total_seconds() / 60)
        delta_y = y2 - y1

        resp_arr = []
        for i in range(0, delta_x, 5):
            timestamp = (x1 + timedelta(hours=time_offset, minutes=i)).strftime(TIME_FORMAT)
            value = (np.cos((float(i) / delta_x) * np.pi) * delta_y / 2 + delta_y / 2) * -1 + y2
            resp_arr.append((timestamp, value))

        return resp_arr

    def interpolate(self, data_arr, time_offset=0.0):
        all_data = []

        prev_x = data_arr[0][0]
        prev_y = data_arr[0][1]
        for i in range(1, len(data_arr)):
            cur_x = data_arr[i][0]
            cur_y = data_arr[i][1]
            all_data.extend(self.interpolator(prev_x, cur_x, prev_y, cur_y, time_offset))
            prev_x = cur_x
            prev_y = cur_y

        return all_data
