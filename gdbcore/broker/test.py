from gdbcore.broker import Broker
from pydq import TIME_FORMAT

from datetime import datetime, timedelta
from uuid import uuid4
import random


class RandomFloat(Broker):
    INTERVAL = 300
    NAME = 'RandomFloat'

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.default_config.update({
            'qids': [str(uuid4()) for i in range(random.randint(40, 70))],
            'cursor': datetime.strftime(datetime.utcnow() - timedelta(days=7), TIME_FORMAT)
        })

    def get_data(self):
        with self.config() as config:
            qids = config['qids']
            cursor = datetime.strptime(config['cursor'], TIME_FORMAT)

        random.seed(cursor.time())
        with self.dq() as dq:
            dq.put_all([{
                'qid': i[0],
                'ts': i[1],
                'val': i[2]}
                       for i in zip(qids, [datetime.utcnow().strftime(TIME_FORMAT) for i in range(len(qids))], [random.random() * 100 for i in range(len(qids))])])

        with self.config() as config:
            config['cursor'] = datetime.utcnow().strftime(TIME_FORMAT)
