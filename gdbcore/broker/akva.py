from gdbcore.broker import Broker
from pydq import TIME_FORMAT

import pandas as pd
import numpy as np
import mysql.connector
from datetime import datetime, timedelta
import pytz
import logging


class AKVABroker(Broker):
    NAME = 'AKVA'
    INTERVAL = 300

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.logger = logging.getLogger('GDB')
        self.default_config.update({'cursor': (datetime.utcnow() - timedelta(days=7)).strftime(TIME_FORMAT),
                                    'username': 'export',
                                    'password': 'akva2018.',
                                    'timezone': 'Canada/Pacific'})

    def get_data(self):
        with self.config() as config:
            cursor, username, password, timezone = config['cursor'], config['username'], config['password'], config['timezone']

        conn = mysql.connector.connect(**{
            'user': username,
            'password': password,
            'host': 'localhost',
            'port': '3131',
            'database': 'piscada_historydata'
        })
        timezone = pytz.timezone(timezone)
        start_time = pytz.utc.localize(datetime.strptime(cursor, TIME_FORMAT)).astimezone(timezone)
        cursor = conn.cursor()
        cursor.execute('SHOW TABLES')
        tables = [t[0] for t in cursor.fetchall()]
        for table in tables:
            # Do one table at a time to help limit MemoryErrors
            stmt = "select * from `{}` where TS > '{}'".format(table, start_time.strftime('%Y-%m-%d %H:%M:%S.%f'))
            df = pd.read_sql(stmt, conn).rename(columns={'NAME': 'qid', 'TS': 'ts', 'Value': 'val'})
            if len(df) == 0:
                continue
            self.logger.info('Found %i new AKVA items' % len(df))
            df['val'] = pd.to_numeric(df['val'], errors='coerce').dropna()
            df['qid'] = self.broker_id(table[:-15].replace(' ', '').strip().lower())
            df = df[df['value'] > 0.1]  # drop 0s
            df = df.groupby(['qid']).resample('5T', on='ts').median()    # downsample to one item per id per 5 min
            df = df.replace([np.inf, -np.inf], np.nan).dropna(subset=['val'])
            df['ts'] = pd.to_datetime(df['ts']).dt.tz_localize(timezone, ambiguous=np.full(len(df), True, dtype=bool))\
                .dt.tz_convert(pytz.utc)
            df.sort_values(by='ts', inplace=True)
            df['ts'] = df['ts'].apply(lambda r: r.strftime(TIME_FORMAT))
            with self.dq() as dq:
                dq.put_from_dataframe(df)

        cursor.close()
        conn.close()

        with self.config() as config:
            config['cursor'] = datetime.strftime(datetime.utcnow(), TIME_FORMAT)
