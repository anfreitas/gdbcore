from gdbcore.broker import Broker
from pydq import TIME_FORMAT

import pyodbc
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import pytz
import logging


class AADIBroker(Broker):
    NAME = 'Steinsvik'
    INTERVAL = 300

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.logger = logging.getLogger('GDB')
        self.default_config.update({'cursor': (datetime.utcnow() - timedelta(days=7)).strftime(TIME_FORMAT),
                                    'server_name': '.\\SQLEXPRESS',
                                    'driver': self.identify_driver(),
                                    'timezone': 'Canada/Pacific'})

    def get_data(self):
        with self.config() as config:
            cursor, server_name, driver, timezone = config['cursor'], config['server_name'], config['driver'], config['timezone']

        if not driver:
            raise Exception('ODBC driver not set')
        odbc = pyodbc.connect(
            driver=driver,
            server=server_name,
            database='aadi_db',
            trusted_connection='yes',
            mars_connection='yes'
        )
        timezone = pytz.timezone(timezone)
        start_time = pytz.utc.localize(datetime.strptime(cursor, TIME_FORMAT)).astimezone(timezone)
        statement = "SELECT data_type_id, date_time, data_value FROM data_values where date_time > '{}';"\
                    .format(start_time.strftime(TIME_FORMAT))
        df = pd.read_sql(statement, odbc).rename(columns={'data_type_id': 'qid',
                                                                'date_time': 'ts',
                                                                'data_value': 'val'})
        if len(df) == 0:
            raise Exception('No new data found in aadi database')
        self.logger.info('Found %i new AADI items' % len(df))
        df['qid'] = df['qid'].apply(self.broker_id)
        df['ts'] = pd.to_datetime(df['ts']).dt.\
            tz_localize(timezone, ambiguous=np.full(len(df), True, dtype=bool)).dt.tz_convert(pytz.utc)
        df.sort_values(by='ts', inplace=True)
        df['ts'] = df['ts'].apply(lambda r: r.strftime(TIME_FORMAT))
        odbc.close()
        with self.dq() as dq:
            dq.put_from_dataframe(df)
        with self.config() as config:
            config['cursor'] = datetime.strftime(datetime.utcnow(), TIME_FORMAT)

    @staticmethod
    def identify_driver():
        driver_list = [
            'ODBC Driver 13 for SQL Server',
            'ODBC Driver 11 for SQL Server',
            'SQL Server Native Client 11.0',
            'SQL Server Native Client 10.0',
            'SQL Native Client'
        ]
        drivers = pyodbc.drivers()
        driver = None
        for d in driver_list:
            if d in drivers:
                driver = d
        if driver is None:
            return ''
        return driver
