from pydq.helpers import DqConfig


class Broker:
    NAME = 'Broker'
    INTERVAL = 30

    def __init__(self, dq_provider):
        self.interval = self.INTERVAL
        self._gid = None
        self.dq_provider = dq_provider
        self.default_config = {'enabled': True}
        self.listeners = {}
        self._dq = None

    def broker_id(self, domain_id):
        return '-'.join((self.NAME, str(domain_id)))

    def run(self):
        self._dq = None
        self.get_data()
        self.run_listeners()

    def get_data(self):
        raise NotImplementedError

    def register_listener(self, listener):
        self.listeners[listener.NAME] = listener

    def run_listeners(self):
        values = list(self.dq().get_all())
        for listener in self.listeners.values():
            with listener.dq() as ldq:
                ldq.put_all(values, forget=True)
            listener.handle_data()

    def config(self):
        return DqConfig(self.NAME, self.dq_provider, '_'.join((self.gid, 'config')), self.default_config)

    @property
    def gid(self):
        if self._gid is None:
            with self.dq_provider('__system__') as dq:
                dq(qid='gid', limit=1)  # load from storage
                if dq.qsize() == 0:
                    raise Exception('No gid set')
                self._gid = dq.get()['val']
        return self._gid

    def dq(self):
        if self._dq is None:
            if self.dq_provider is not None:
                self._dq = self.dq_provider('_'.join((self.gid, self.NAME)))
            else:
                raise Exception('No data queue class defined for broker %s' % self.NAME)
        return self._dq
