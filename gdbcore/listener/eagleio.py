from gdbcore.listener.publish import Publisher

import requests
from ftplib import FTP
from io import BytesIO
import json
from socket import timeout as socket_timeout
from time import sleep
import logging


class EagleioListener(Publisher):
    NAME = 'eagleio'
    INTERVAL = 10

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.logger = logging.getLogger('GDB')

    def publish(self, data_chunk, endpoint):
        api_key = endpoint['api_key']
        node_map = endpoint['node_map']
        for bid in set([i[0] for i in data_chunk]):
            if bid not in node_map:
                node_map[bid] = {}

        columns = list(set([i[0] for i in data_chunk]))
        nid_map = {bid: node['id'] for bid, node in node_map.items()}
        params = {
            'params': ','.join(
                ['%s(columnIndex:%i)' % (nid_map[column], idx) for idx, column in enumerate(columns)]),
        }

        data_arr = {ts: {'ts': ts, 'f': {}} for ts in set([i[1] for i in data_chunk])}
        for item in data_chunk:
            bid, ts, val = item
            if not node_map[bid]:
                continue  # hasn't been configured yet
            if node_map[bid]['dataType'] == 'TEXT':
                val = str(val)
            data_obj = data_arr[ts]
            nid_index = columns.index(bid)
            data_obj['f'][str(nid_index)] = val

        body = {
            'docType': 'jts',
            'version': '1.0',
            'data': data_arr
        }

        headers = {'X-Api-Key': api_key, 'Content-Type': 'application/json'}

        resp = requests.put('https://api.eagle.io/api/v1/historic', params=params, headers=headers, json=body)
        self.logger.info('Sent %i data points to eagle.io' % len(data_chunk))
        if resp.status_code != 202:
            raise Exception('Failed sending to Eagle.io: %i\n%s' % (resp.status_code, resp.text))

    def get_nodes(self, datasource_id):
        with self.config() as config:
            api_key = config['api_key']

        params = {'filter': 'parentId($eq:%s)' % datasource_id}
        return requests.get('https://api.eagle.io/api/v1/nodes', params=params,
                            headers={'X-Api-Key': api_key, 'Content-Type': 'application/json'}).json()

    def update_datasource(self, datasource_id, ftp_user, ftp_password):
        with self.config() as config:
            api_key, node_map = config['api_key'], config['node_map']

        json_body = {
            'subType': 'TIMESERIES',
            'version': '1.0',
            'data': [],
            'docType': 'jts',
            'header': {'columns': {
                idx: {
                    'dataType': row['dataType'],
                    'renderType': 'VALUE',
                    'aggregate': 'NONE',
                    'name': row['name'],
                    'units': '',
                    'id': row['nid']
                } for idx, row in enumerate(node_map.items())
            }}
        }

        retries = 0
        while retries < 5:
            try:
                ftp = FTP('ftp.eagle.io', user=ftp_user, passwd=ftp_password, timeout=30)
                with BytesIO(json.dumps(json_body).encode('utf-8')) as f:
                    ftp.storbinary('STOR output.json', f)
                ftp.quit()
                break
            except socket_timeout:
                retries += 1
                if retries == 5:
                    raise Exception('Failed to update eagle.io datasource after 5 tries')

        retries = 0
        nodes = self.get_nodes(datasource_id)
        while retries < 5:
            try:
                assert len(nodes) == len(node_map)
            except AssertionError:
                retries += 1
                if retries == 5:
                    raise Exception('Timed out waiting for datasource to add new nodes')
                sleep(2)
                nodes = self.get_nodes(datasource_id)

        nodes_by_name = {n['name']: n for n in nodes}
        nodes_by_id = {n['_id']: n for n in nodes}
        for row in node_map.values():
            if not row['nid'] and row['name'] in nodes_by_name:
                row['nid'] = nodes_by_name[row['name']]['_id']
            if row['name'] not in nodes_by_name and row['nid'] in nodes_by_id:
                requests.put('https://api.eagle.io/api/v1/nodes/%s' % row['nid'],
                             headers={'X-Api-Key': api_key, 'Content-Type': 'application/json'},
                             json={'name': row['name']})
        with self.config() as config:
            config['node_map'] = node_map
