from gdbcore.listener.publish import Publisher

import requests
import logging


class GDBShare(Publisher):
    NAME = 'GDBShare'

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.logger = logging.getLogger('GDB')

    def publish(self, data_chunk, endpoint):
        resp = requests.put(endpoint['url'], json=data_chunk)
        self.logger.info('Sent %i data points to %s' % (len(data_chunk), endpoint['url']))
        if resp.status_code != 202:
            raise Exception('Failed sending to %s: %s' % (endpoint['url'], resp.text))
