from pydq.helpers import DqConfig


class Listener:
    NAME = 'Listener'

    def __init__(self, dq_provider):
        self.dq_provider = dq_provider
        self._gid = None
        self.default_config = {'enabled': True}
        self._dq = None

    def listener_id(self, domain_id):
        return '-'.join((self.NAME, str(domain_id)))

    def handle_data(self):
        raise NotImplementedError

    def config(self):
        return DqConfig(self.NAME, self.dq_provider, '_'.join((self.gid, 'config')), self.default_config)

    @property
    def gid(self):
        if self._gid is None:
            with self.dq_provider('__system__') as dq:
                dq(qid='gid', limit=1)  # load from storage
                if dq.qsize() == 0:
                    raise Exception('No gid set')
                self._gid = dq.get()['val']
        return self._gid

    def dq(self):
        if self._dq is None:
            if self.dq_provider is not None:
                self._dq = self.dq_provider('_'.join((self.gid, self.NAME)))
            else:
                raise Exception('No data queue class defined for broker %s' % self.NAME)
        return self._dq
