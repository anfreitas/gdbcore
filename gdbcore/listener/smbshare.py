from gdbcore.listener.publish import Publisher
from pydq.helpers import TIME_FORMAT

import requests
import logging
from datetime import datetime


class SMBShare(Publisher):
    NAME = 'SMBShare'

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.logger = logging.getLogger('GDB')

    def publish(self, data_chunk, endpoint):
        with self.dq_provider('__system__') as dq:
            dq(qid='smb_api_key', limit=1)
            if dq.qsize() == 0:
                raise Exception('No smb_api_key set in __system__ table')
            api_key = dq.get()['val']
        data_chunk = [{
            'id': d['qid'],
            'measured_at': str(datetime.strptime(d['ts'], TIME_FORMAT)),
            'value': d['val']
        } for d in data_chunk]
        resp = requests.put(endpoint['url'], json=data_chunk, headers={'X-API-KEY': api_key})
        self.logger.info('Sent %i data points to %s' % (len(data_chunk), endpoint['url']))
        if resp.status_code != 202:
            raise Exception('Failed sending to %s: %s' % (endpoint['url'], resp.text))
