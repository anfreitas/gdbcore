from gdbcore.listener import Listener


class Publisher(Listener):
    NAME = 'Publisher'
    CHUNK_SIZE = 1000

    def __init__(self, dq_provider):
        super().__init__(dq_provider)
        self.default_config.update({'endpoints': []})

    def publish(self, data_chunk, endpoint):
        raise NotImplementedError

    def handle_data(self):
        with self.config() as config:
            endpoints = config['endpoints']

        with self.dq() as dq:
            values = list(dq.get_all())

        for endpoint in endpoints:
            chunk = []
            for value in values:
                chunk.append(value)
                if len(chunk) >= self.CHUNK_SIZE:
                    self.publish(chunk, endpoint)
                    chunk.clear()
            if len(chunk) > 0:
                self.publish(chunk, endpoint)
