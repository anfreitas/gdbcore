from setuptools import setup, find_packages

with open('README.md', 'r') as fh:
    long_description = fh.read()

setup(
    name='gdbcore',
    version='0.0.36',
    author='Andrew Freitas',
    author_email='andrewfreitas09@gmail.com',
    description='Core of GDB application including all available brokers',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/anfreitas/gdbcore',
    packages=find_packages(),
    install_requires=["pyodbc==4.0.27", "mysql-connector-python", "bs4", "pandas", "numpy", "requests", "pytz", "pydq"],
    extras_require={"aws": ["boto3"]},
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
